#include "Float.hpp"
#include "AbstractVM.hpp"
#include "OperationException.hpp"
#include "OperationTemplates.hpp"
#include <limits>
#include <cmath>

Float::Float(std::string const &num) : _type(FLOAT) {
	float	integer = std::strtof(num.c_str(), NULL);

	if (integer == HUGE_VALF)
	{
		if (num.find('-') == std::string::npos)
			throw (OperationException::Overflow());
		else
			throw (OperationException::Underflow());
	}
	this->_number = std::to_string(integer);
}

Float::Float(Float const &obj) : _type(FLOAT), _number(obj.toString()) {}

Float::~Float() {}

int					Float::getPrecision() const {
	return (this->_type);
}

eOperandType		Float::getType() const {
	return (this->_type);
}

IOperand const		*Float::operator+(IOperand const &obj) const {
	float		p1 = std::stof(this->toString());
	float		p2 = std::stof(obj.toString());
	float		sum;

	sum = add(p1, p2);
	return (createOperand(FLOAT, std::to_string(sum)));
}

IOperand const		*Float::operator-(IOperand const &obj) const {
	float		p1 = std::stof(this->toString());
	float		p2 = std::stof(obj.toString());
	float		sum;

	sum = sub(p1, p2);
	return (createOperand(FLOAT, std::to_string(sum)));
}

IOperand const		*Float::operator*(IOperand const &obj) const {
	float		p1 = std::stof(this->toString());
	float		p2 = std::stof(obj.toString());
	float		sum;

	sum = mul(p1, p2);
	return (createOperand(FLOAT, std::to_string(sum)));
}

IOperand const		*Float::operator/(IOperand const &obj) const {
	float		p1 = std::stof(this->toString());
	float		p2 = std::stof(obj.toString());
	float		sum;

	sum = divka(p1, p2);
	return (createOperand(FLOAT, std::to_string(sum)));
}

IOperand const		*Float::operator%(IOperand const &) const {
	throw (OperationException("Mod doesn't work with floating point numbers"));
}

std::string const	&Float::toString(void) const {
	return (this->_number);
}
