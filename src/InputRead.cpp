#include "InputRead.hpp"
#include <fstream>
#include <iostream>
#include "InlineFuncs.hpp"
#include <regex>
#include "ErrorList.hpp"

/*
** Static vars
*/

bool    InputRead::Lexer::_hasMistakes = false;
bool    InputRead::Lexer::_exit = false;

/*
** Methods
*/

InputRead::InputRead() : _list(std::list<s_line>()) {}

InputRead::~InputRead() {}

bool		        InputRead::getInput(const char *str) {
	std::ifstream	stream;

	stream.open(str);
	if (!stream.is_open() || !stream.good())
		throw (std::runtime_error("Cannot open file"));

	char		buff[512];
	std::size_t	i = 0;
	std::string	string;
	s_line		line;

	while (stream.getline(buff, 512)) {
		string = buff;
		trim(string);
		string = string.substr(0, string.find(";"));
		trim(string);
		if (Lexer::validate(string, i))
			string = reconstructString(string);
		line = {string, i, string.empty()};
		this->_list.push_back(line);
		++i;
	}
	this->_iter = this->_list.cbegin();
	return (InputRead::Lexer::hasMistakes());
}

bool                InputRead::getInput() {
	char		buff[512];
	std::size_t	i = 0;
	std::string	string;
	s_line		line;

	while (std::cin.getline(buff, 512)) {
		string = buff;
		trim(string);
		if (string == ";;")
			break;
		string = string.substr(0, string.find(";"));
		trim(string);
		if (Lexer::validate(string, i))
			string = reconstructString(string);
		line = {string, i, string.empty()};
		this->_list.push_back(line);
		++i;
	}
	this->_iter = this->_list.cbegin();
	return (InputRead::Lexer::hasMistakes());
}

bool                InputRead::getLine(Line &line) {
	if (this->_iter == this->_list.cend())
		return (false);
	line = (*this->_iter);
	++(this->_iter);
	return (true);
}

std::string			InputRead::reconstructString(std::string const &str) const {
	size_t		push = str.find("push");
	size_t		assert = str.find("assert");
	std::string	operation;
	std::string	numType;
	std::string	number;

	if (push != std::string::npos)
		operation = "push ";
	else if (assert != std::string::npos)
		operation = "assert ";
	else
		return (str);
	numType = str.substr(operation.size(), str.find("(") - operation.size());
	number = str.substr(str.find("(") + 1, str.find(")") - str.find("(") - 1);
	return (operation + numType + " " + number);
}

/*
** Lexer
*/

bool                InputRead::Lexer::hasMistakes() {
	if (InputRead::Lexer::_exit == false) {
		ExitError();
		return (true);
	}
	return (InputRead::Lexer::_hasMistakes);
}

bool                InputRead::Lexer::validate(std::string const &str, std::size_t line) {
	std::regex      expression("^((pop|dump|add|sub|mul|div|mod|print|exit|clear|dumpone|infoone|info|uptype)|((push|assert) (int8|int16|int32|float|double)[(][-+]?\\d+([.]?\\d+[f]?)?[)]?))$");

	if (str.empty())
		return (false);
	if (!std::regex_match(str, expression)) {
		InputRead::Lexer::_hasMistakes = true;
		if (!InputRead::Lexer::deepCheck(str, line + 1))
			addError("Lexer error: Undefined symbols", line + 1, str);
		return (false);
	}
	if (str == "exit")
		InputRead::Lexer::_exit = true;
	return (true);
}

bool				InputRead::Lexer::deepCheck(std::string const &str, std::size_t line) {
	if (std::regex_match(str,
std::regex("^((pop|dump|add|sub|mul|div|mod|print|exit|clear|dumpone|infoone|info|uptype)|((push|assert)[ ]+(int8|int16|int32|float|double)[(][-+]?\\d+([.]?\\d+[f]?)?[)]?))$")))
	{
		addError("Lexer error: extra spaces between push/assert and number", line, str);
		return (true);
	}
	else if (!std::regex_match(str,
std::regex("^(pop|dump|add|sub|mul|div|mod|print|exit|push|assert|clear|dumpone|infoone|info|uptype)")) &&
			std::regex_match(str,
std::regex(".+(pop|dump|add|sub|mul|div|mod|print|exit|push|assert|clear|dumpone|infoone|info|uptype)")))
	{
		addError("Lexer error: undefined symbols before command", line, str);
		return (true);
	}
	else if (std::regex_match(str, std::regex("^(pop|dump|add|sub|mul|div|mod|print|exit|clear|dumpone|infoone|info|uptype).+")))
	{
		addError("Lexer error: this command cannot have parameters", line, str);
		return (true);
	}
	else if (std::regex_match(str, std::regex("^(push|assert) (int8|int16|int32|float|double)[(][-+]?\\d+([.]?\\d+[f]?)?[)]?.+")))
	{
		addError("Lexer error: this command cannot have more than two parameters", line, str);
		return (true);
	}
	else if (std::regex_match(str, std::regex("^(push|assert)[]*$")))
	{
		addError("Lexer error: this command must have parameter", line, str);
		return (true);
	}
	else if (std::regex_match(str, std::regex(std::regex("^(pop|dump|add|sub|mul|div|mod|print|exit|push|assert|clear|dumpone|infoone|info|uptype).+"))))
	{
		addError("Lexer error: undefined symbols after command", line, str);
		return (true);
	}
	return (false);
}
