#include "OperationException.hpp"

OperationException::OperationException(std::string const &str) : _str(str) {}

OperationException::OperationException(OperationException const &obj) : _str(obj._str) {}

OperationException::~OperationException() throw() {}

OperationException	&OperationException::operator=(OperationException const &) {
	return (*this);
}

const char *		OperationException::what() const throw() {
	return (this->_str.c_str());
}

/*
*/

OperationException::DivisionByZero::DivisionByZero() : _str("Division by zero") {}

OperationException::DivisionByZero::DivisionByZero(DivisionByZero const &obj) : _str(obj._str) {}

OperationException::DivisionByZero::~DivisionByZero() throw() {}

OperationException::DivisionByZero	&OperationException::DivisionByZero::operator=(DivisionByZero const &) {
	return (*this);
}

const char *		OperationException::DivisionByZero::what() const throw() {
	return (this->_str.c_str());
}

/*
*/

OperationException::Overflow::Overflow() : _str("Number overflowed") {}

OperationException::Overflow::Overflow(Overflow const &obj) : _str(obj._str) {}

OperationException::Overflow::~Overflow() throw() {}

OperationException::Overflow	&OperationException::Overflow::operator=(Overflow const &) {
	return (*this);
}

const char *		OperationException::Overflow::what() const throw() {
	return (this->_str.c_str());
}

/*
*/

OperationException::Underflow::Underflow() : _str("Number underflowed") {}

OperationException::Underflow::Underflow(Underflow const &obj) : _str(obj._str) {}

OperationException::Underflow::~Underflow() throw() {}

OperationException::Underflow	&OperationException::Underflow::operator=(Underflow const &) {
	return (*this);
}

const char *		OperationException::Underflow::what() const throw() {
	return (this->_str.c_str());
}

/*
*/

OperationException::WrongNumber::WrongNumber() : _str("Undefined symbols in number representation") {}

OperationException::WrongNumber::WrongNumber(WrongNumber const &obj) : _str(obj._str) {}

OperationException::WrongNumber::~WrongNumber() throw() {}

OperationException::WrongNumber	&OperationException::WrongNumber::operator=(WrongNumber const &) {
	return (*this);
}

const char *		OperationException::WrongNumber::what() const throw() {
	return (this->_str.c_str());
}

/*
*/

OperationException::EmptyStack::EmptyStack() : _str("Do not have enough numbers in stack") {}

OperationException::EmptyStack::EmptyStack(EmptyStack const &obj) : _str(obj._str) {}

OperationException::EmptyStack::~EmptyStack() throw() {}

OperationException::EmptyStack	&OperationException::EmptyStack::operator=(EmptyStack const &) {
	return (*this);
}

const char *		OperationException::EmptyStack::what() const throw() {
	return (this->_str.c_str());
}

/*
*/

OperationException::AssertError::AssertError() : _str("Assert failed") {}

OperationException::AssertError::AssertError(AssertError const &obj) : _str(obj._str) {}

OperationException::AssertError::~AssertError() throw() {}

OperationException::AssertError	&OperationException::AssertError::operator=(AssertError const &) {
	return (*this);
}

const char *		OperationException::AssertError::what() const throw() {
	return (this->_str.c_str());
}

/*
*/

OperationException::PrintError::PrintError() : _str("Number is not Int8 type for print") {}

OperationException::PrintError::PrintError(PrintError const &obj) : _str(obj._str) {}

OperationException::PrintError::~PrintError() throw() {}

OperationException::PrintError	&OperationException::PrintError::operator=(PrintError const &) {
	return (*this);
}

const char *		OperationException::PrintError::what() const throw() {
	return (this->_str.c_str());
}
