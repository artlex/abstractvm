#include "Int16.hpp"
#include "AbstractVM.hpp"
#include <exception>
#include <limits>
#include "OperationException.hpp"
#include "OperationTemplates.hpp"

Int16::Int16(std::string const &num) : _type(INT16) {
	long	integer = std::strtol(num.c_str(), NULL, 0);

	if (errno == ERANGE)
	{
		if (integer == LONG_MAX)
			throw (OperationException::Overflow());
		else if (integer == LONG_MIN)
			throw (OperationException::Underflow());
	}
	if (integer > std::numeric_limits<short>::max())
		throw (OperationException::Overflow());
	else if (integer < std::numeric_limits<short>::min())
		throw (OperationException::Underflow());
	this->_number = std::to_string(integer);
}

Int16::Int16(Int16 const &obj) : _type(INT16), _number(obj.toString()) {}

Int16::~Int16() {}

int					Int16::getPrecision() const {
	return (this->_type);
}

eOperandType		Int16::getType() const {
	return (this->_type);
}

IOperand const		*Int16::operator+(IOperand const &obj) const {
	short	p1 = std::stoi(this->toString());
	short	p2 = std::stoi(obj.toString());
	short	sum;

	sum = add(p1, p2);
	return (createOperand(INT16, std::to_string(sum)));
}

IOperand const		*Int16::operator-(IOperand const &obj) const {
	short		p1 = std::stoi(this->toString());
	short		p2 = std::stoi(obj.toString());
	short		sum;

	sum = sub(p1, p2);
	return (createOperand(INT16, std::to_string(sum)));
}

IOperand const		*Int16::operator*(IOperand const &obj) const {
	short		p1 = std::stoi(this->toString());
	short		p2 = std::stoi(obj.toString());
	short		sum;

	sum = mul(p1, p2);
	return (createOperand(INT16, std::to_string(sum)));
}

IOperand const		*Int16::operator/(IOperand const &obj) const {
	short		p1 = std::stoi(this->toString());
	short		p2 = std::stoi(obj.toString());
	short		sum;

	sum = divka(p1, p2);
	return (createOperand(INT16, std::to_string(sum)));
}

IOperand const		*Int16::operator%(IOperand const &obj) const {
	short		p1 = std::stoi(this->toString());
	short		p2 = std::stoi(obj.toString());
	short		sum;

	sum = mod(p1, p2);
	return (createOperand(INT16, std::to_string(sum)));
}

std::string const	&Int16::toString(void) const {
	return (this->_number);
}
