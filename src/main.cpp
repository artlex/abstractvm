/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/12 19:11:51 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/12 19:40:29 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cstdio>
#include <iostream>
#include "InputRead.hpp"
#include "ErrorList.hpp"
#include "OperationHandler.hpp"

static void	help() {
	std::cout << "AbstractVM. What is AbstractVM? It is virtual machine. Google it." << std::endl <<
		"There you can see regular expression that has to help you understand what commands you can use and how." << std::endl <<
		"More precisely there is information about what you can type in ONE LINE" << std::endl <<
		"	(^[ ]*\'INSTRUCTION\'( \'ARGUMENT\')?[ ]*;*$)|()" << std::endl <<
		"HARD? WOAAHAHAHAHHAHAHHAH!!!! I will explain this if you need." << std::endl <<
		"Now I have to write what values can have \'INSTRUCTION\' and \'ARGUMENT\':" << std::endl <<
		"\'INSTRUCTION: \' push, pop, dump, assert, add ,sub, mul, div, mod, print, exit, clear, dumpone, infowone, info, uptype" << std::endl <<
		"All instructions except push and exit need non-empty stack" << std::endl <<
		"Push and assert MUST have argument. Other command MUST NOT have it. Remember it!" << std::endl <<
		"There is regular expression that describes \'INSTRUCTION:\'" << std::endl <<
		"	(int8|int16|int32|float|double)[(][-+]?\\d+([.]?\\d+[f]?)?[)]" << std::endl <<
		"Thats all. All other information I will explain by myself." << std::endl;
}

static void	usage() {
	std::cout <<
		"usage: \x1b[32m\"./avm\" or \"./avm filename\" or \"./avm < filename\"\x1b[0m" <<
		std::endl << "Type \"./avm --help\" to see more info" << std::endl;
}

void    	do_operations(InputRead &info) {
	Line                line;
	OperationHandler	obj;

	try {
		while (info.getLine(line))
			obj.factory(line);
	}
	catch (...) {
		printErrors();
	}
}

int			main(int argc, char **argv)
{
	try {
		if (argc > 2) {
			usage();
			return (0);
		}
		InputRead	info;
		bool        res = false;

		if (argc == 2)
		{
			if (!std::strcmp(argv[1], "--help"))
				help();
			res = info.getInput(argv[1]);
		}
		else
		{
			res = info.getInput();
			std::cout << std::endl << "___" << std::endl << std::endl;
		}
		if (res) {
			printErrors();
			exit(1);
		}
		do_operations(info);
	}
	catch (std::exception &obj) {
		std::cout << obj.what() << std::endl;
		exit(1);
	}
}
