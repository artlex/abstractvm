#include "ErrorList.hpp"
#include <iostream>

ErrorList	*ErrorList::_ptr = nullptr;

ErrorList	*ErrorList::getErrorList() {
	if (ErrorList::_ptr == nullptr)
		ErrorList::_ptr = new ErrorList();
	return (ErrorList::_ptr);
}

ErrorList::ErrorList() : _list(std::list<std::string>()) {}

ErrorList::~ErrorList() {}

void				ErrorList::push(std::string const &str) {
	this->_list.push_back(str);
}

void                ErrorList::printErrors() const {
	for (auto iter = this->_list.begin(); iter != this->_list.end(); ++iter)
		std::cerr << *iter << std::endl;
}
