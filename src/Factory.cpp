#include "IOperand.hpp"
#include "AbstractVM.hpp"
#include <vector>

#include "Int8.hpp"
#include "Int16.hpp"
#include "Int32.hpp"
#include "Float.hpp"
#include "Double.hpp"

IOperand const	*createInt8(std::string const & value) {
	return (new Int8(value));
}

IOperand const	*createInt16(std::string const & value) {
	return (new Int16(value));
}

IOperand const	*createInt32(std::string const & value) {
	return (new Int32(value));
}

IOperand const	*createFloat(std::string const & value) {
	return (new Float(value));
}

IOperand const	*createDouble(std::string const & value) {
	return (new Double(value));
}

const IOperand	*createOperand(eOperandType type, std::string const &value)
{
	std::vector<IOperand const *(*)(std::string const &)>	myvector;

	myvector.push_back(&createInt8);
	myvector.push_back(&createInt16);
	myvector.push_back(&createInt32);
	myvector.push_back(&createFloat);
	myvector.push_back(&createDouble);
	return (myvector[type](value));
}
