#include "Double.hpp"
#include "AbstractVM.hpp"
#include "OperationException.hpp"
#include "OperationTemplates.hpp"
#include <limits>
#include <cmath>

Double::Double(std::string const &num) : _type(DOUBLE) {
	double	integer = std::strtod(num.c_str(), NULL);

	if (integer == HUGE_VAL)
	{
		if (num.find('-') == std::string::npos)
			throw (OperationException::Overflow());
		else
			throw (OperationException::Underflow());
	}
	this->_number = std::to_string(integer);
}

Double::Double(Double const &obj) : _type(DOUBLE), _number(obj.toString()) {}

Double::~Double() {}

int					Double::getPrecision() const {
	return (this->_type);
}

eOperandType		Double::getType() const {
	return (this->_type);
}

IOperand const		*Double::operator+(IOperand const &obj) const {
	double		p1 = std::stod(this->toString());
	double		p2 = std::stod(obj.toString());
	double		sum;

	sum = add(p1, p2);
	return (createOperand(DOUBLE, std::to_string(sum)));
}

IOperand const		*Double::operator-(IOperand const &obj) const {
	double		p1 = std::stod(this->toString());
	double		p2 = std::stod(obj.toString());
	double		sum;

	sum = sub(p1, p2);
	return (createOperand(DOUBLE, std::to_string(sum)));
}

IOperand const		*Double::operator*(IOperand const &obj) const {
	double		p1 = std::stod(this->toString());
	double		p2 = std::stod(obj.toString());
	double		sum;

	sum = mul(p1, p2);
	return (createOperand(DOUBLE, std::to_string(sum)));
}

IOperand const		*Double::operator/(IOperand const &obj) const {
	double		p1 = std::stod(this->toString());
	double		p2 = std::stod(obj.toString());
	double		sum;

	sum = divka<double>(p1, p2);
	return (createOperand(DOUBLE, std::to_string(sum)));
}

IOperand const		*Double::operator%(IOperand const &) const {
	throw (OperationException("Mod doesn't work with floating point numbers"));
}

std::string const	&Double::toString(void) const {
	return (this->_number);
}
