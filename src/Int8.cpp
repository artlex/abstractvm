#include "Int8.hpp"
#include "AbstractVM.hpp"
#include "OperationException.hpp"
#include <limits>
#include "OperationTemplates.hpp"

Int8::Int8(std::string const &num) : _type(INT8) {
	long	integer = std::strtol(num.c_str(), NULL, 0);

	if (errno == ERANGE)
	{
		if (integer == LONG_MAX)
			throw (OperationException::Overflow());
		else if (integer == LONG_MIN)
			throw (OperationException::Underflow());
	}
	if (integer > std::numeric_limits<char>::max())
		throw (OperationException::Overflow());
	else if (integer < std::numeric_limits<char>::min())
		throw (OperationException::Underflow());
	this->_number = std::to_string(integer);
}

Int8::Int8(Int8 const &obj) : _type(INT8), _number(obj.toString()) {}

Int8::~Int8() {}

int					Int8::getPrecision() const {
	return (this->_type);
}

eOperandType		Int8::getType() const {
	return (this->_type);
}

IOperand const		*Int8::operator+(IOperand const &obj) const {
	char	p1 = std::stoi(this->toString());
	char	p2 = std::stoi(obj.toString());
	char	sum;

	sum = add(p1, p2);
	return (createOperand(INT8, std::to_string(sum)));
}

IOperand const		*Int8::operator-(IOperand const &obj) const {
	char		p1 = std::stoi(this->toString());
	char		p2 = std::stoi(obj.toString());
	char		sum;

	sum = sub(p1, p2);
	return (createOperand(INT8, std::to_string(sum)));
}

IOperand const		*Int8::operator*(IOperand const &obj) const {
	char		p1 = std::stoi(this->toString());
	char		p2 = std::stoi(obj.toString());
	char		sum;

	sum = mul(p1, p2);
	return (createOperand(INT8, std::to_string(sum)));
}

IOperand const		*Int8::operator/(IOperand const &obj) const {
	char		p1 = std::stoi(this->toString());
	char		p2 = std::stoi(obj.toString());
	char		sum;

	sum = divka(p1, p2);
	return (createOperand(INT8, std::to_string(sum)));
}

IOperand const		*Int8::operator%(IOperand const &obj) const {
	char		p1 = std::stoi(this->toString());
	char		p2 = std::stoi(obj.toString());
	char		sum;

	sum = mod(p1, p2);
	return (createOperand(INT8, std::to_string(sum)));
}

std::string const	&Int8::toString(void) const {
	return (this->_number);
}
