/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OperationHandler.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 16:49:59 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/12 19:09:13 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <OperationHandler.hpp>
#include "OperationHandler.hpp"
#include "ErrorList.hpp"
#include "IOperand.hpp"
#include "AbstractVM.hpp"
#include <sstream>
#include <iostream>
#include <OperationException.hpp>

/*
** Constructor/Destructor
*/

OperationHandler::OperationHandler() : _list(std::list<const IOperand *>()),
										_exit(false), _err(false) {}

OperationHandler::~OperationHandler() {
	for (auto i = this->_list.begin(); i != this->_list.end(); ++i)
		delete *i;
}

/*
** Help
*/

void            OperationHandler::_getNumbers(IOperand const *&p1, IOperand const *&p2) const {
	if (this->_list.size() < 2)
		throw(OperationException::EmptyStack());

	auto	iter = this->_list.rbegin();
	p1 = *iter;
	++iter;
	p2 = *iter;
}

IOperand const	*OperationHandler::_changePrecision(IOperand const *p1, IOperand const *p2) const {
	if (p1->getType() < p2->getType())
		return (createOperand(p2->getType(), p1->toString()));
	return (nullptr);
}

void			OperationHandler::_updateStack(IOperand const *&num) {
	delete this->_list.back();
	this->_list.pop_back();
	delete this->_list.back();
	this->_list.pop_back();
	this->_list.push_back(num);
}

bool		OperationHandler::wasExit() {
	return (this->_exit);
}

bool		OperationHandler::wasErr() {
	return (this->_err);
}

/*
** Operations
*/

void            OperationHandler::push(std::string const &str) {
	std::stringstream		stream(str);
	std::string             type;
	std::string             number;
	std::string             strarr[5] = {"int8", "int16", "int32", "float", "double"};
	eOperandType            typearr[5] = {INT8, INT16, INT32, FLOAT, DOUBLE};
	eOperandType            operand = INT8;
	const IOperand          *ptr;

	stream >> type;
	stream >> number;
	for (unsigned int i = 0; i < 5; ++i) {
		if (type == strarr[i]) {
			operand = typearr[i];
			break;
		}
	}
	ptr = createOperand(operand, number);
	this->_list.push_back(ptr);
}

void            OperationHandler::assert(std::string const &str) {
	if (this->_list.size() < 1)
		throw(OperationException("Assert on empty stack"));

	std::stringstream		stream(str);
	std::string             type;
	std::string             number;
	std::string             strarr[5] = {"int8", "int16", "int32", "float", "double"};
	eOperandType            typearr[5] = {INT8, INT16, INT32, FLOAT, DOUBLE};
	eOperandType            operand = INT8;

	stream >> type;
	stream >> number;
	for (unsigned int i = 0; i < 5; ++i) {
		if (type == strarr[i]) {
			operand = typearr[i];
			break;
		}
	}
	const eOperandType		etype = this->_list.back()->getType();
	const std::string		stack_str = this->_list.back()->toString();
	bool					result = false;

	if (etype != operand)
		throw(OperationException::AssertError());
	if (etype == INT8 || etype == INT16 || etype == INT32)
		result = std::stol(stack_str) == std::stol(number);
	else if (etype == FLOAT || etype == DOUBLE)
		result = std::stod(stack_str) == std::stod(number);
	if (result == false)
		throw(OperationException::AssertError());
}

void            OperationHandler::pop() {
	if (this->_list.size() < 1)
		throw(OperationException("Pop empty stack"));
	this->_list.pop_back();
}

void            OperationHandler::dump() {
	if (this->_list.size() < 1)
		throw(OperationException("Dump empty stack"));
	if (this->_err)
		return ;
	for (auto i = this->_list.rbegin(); i != this->_list.rend(); ++i)
		std::cout << (*i)->toString() << std::endl;
}

void            OperationHandler::print() {
	if (this->_list.size() < 1)
		throw(OperationException("Print empty stack"));
	if (this->_list.back()->getType() != INT8)
		throw(OperationException::PrintError());
	if (this->_err)
		return ;
	std::cout <<
		static_cast<unsigned char>(std::stoi(this->_list.back()->toString(), NULL, 10))
		<< std::endl;
}

void            OperationHandler::exit() {
	this->_exit = true;
}

void            OperationHandler::add() {
	IOperand const  *p1 = nullptr;
	IOperand const  *p2 = nullptr;
	IOperand const  *p3 = nullptr;
	IOperand const  *p4 = nullptr;

	this->_getNumbers(p1, p2);
	if ((p4 = this->_changePrecision(p2, p1)))
	{
		p3 = *p4 + *p1;
		delete p4;
	}
	else
		p3 = *p2 + *p1;
	this->_updateStack(p3);
}

void            OperationHandler::sub() {
	IOperand const  *p1 = nullptr;
	IOperand const  *p2 = nullptr;
	IOperand const  *p3 = nullptr;
	IOperand const  *p4 = nullptr;

	this->_getNumbers(p1, p2);
	if ((p4 = this->_changePrecision(p2, p1)))
	{
		p3 = *p4 - *p1;
		delete p4;
	}
	else
		p3 = *p2 - *p1;
	this->_updateStack(p3);
}

void            OperationHandler::mul() {
	IOperand const  *p1 = nullptr;
	IOperand const  *p2 = nullptr;
	IOperand const  *p3 = nullptr;
	IOperand const  *p4 = nullptr;

	this->_getNumbers(p1, p2);
	if ((p4 = this->_changePrecision(p2, p1)))
	{
		p3 = *p4 * *p1;
		delete p4;
	}
	else
		p3 = *p2 * *p1;
	this->_updateStack(p3);
}

void            OperationHandler::div() {
	IOperand const  *p1 = nullptr;
	IOperand const  *p2 = nullptr;
	IOperand const  *p3 = nullptr;
	IOperand const  *p4 = nullptr;

	this->_getNumbers(p1, p2);
	if ((p4 = this->_changePrecision(p2, p1)))
	{
		p3 = *p4 / *p1;
		delete p4;
	}
	else
		p3 = *p2 / *p1;
	this->_updateStack(p3);
}

void            OperationHandler::mod() {
	IOperand const  *p1 = nullptr;
	IOperand const  *p2 = nullptr;
	IOperand const  *p3 = nullptr;
	IOperand const  *p4 = nullptr;

	this->_getNumbers(p1, p2);
	if ((p4 = this->_changePrecision(p2, p1)))
	{
		p3 = *p4 % *p1;
		delete p4;
	}
	else
		p3 = *p2 % *p1;
	this->_updateStack(p3);
}

/*
** Factory
*/

void		OperationHandler::factory(Line &line) {
	if (this->_exit == true)
		return ;
	typedef void	(OperationHandler::*Operations)();
	const int		elems = 14;

	std::string	s1 = line.str.substr(0, line.str.find(" "));
	std::string	s2 = line.str.substr(line.str.find(" ") + 1, line.str.size());
	std::string	strarr[] = {"pop", "dump", "add", "sub", "mul", "div", "mod", "print", "exit",
		"clear", "dumpone", "infoone", "info", "uptype"};
	Operations	oper[]= {&OperationHandler::pop, &OperationHandler::dump, &OperationHandler::add,
				&OperationHandler::sub, &OperationHandler::mul, &OperationHandler::div,
				&OperationHandler::mod, &OperationHandler::print, &OperationHandler::exit,
				&OperationHandler::clear, &OperationHandler::dumpone, &OperationHandler::infoone,
				&OperationHandler::info, &OperationHandler::uptype};

	try {
		if (s1 == "push") {
			this->push(s2);
			return ;
		}
		else if (s1 == "assert") {
			this->assert(s2);
			return ;
		}
		for (int i = 0; i < elems; ++i) {
			if (s1 == strarr[i]) {
				(this->*(oper[i]))();
				return ;
			}
		}
	}
	catch(std::exception &obj) {
		this->_err = true;
		addError(obj.what(), line.line + 1, line.str);
		throw;
	}
}

/*
** Bonus operations
*/

void            OperationHandler::clear() {
	if (this->_list.size() < 1)
		throw(OperationException("Clear empty stack"));
	if (this->_err)
		return ;
	this->_list = t_lst();
}

void            OperationHandler::dumpone() {
	if (this->_list.size() < 1)
		throw(OperationException("Dumpone on empty stack"));
	if (this->_err)
		return ;
	std::cout << this->_list.back()->toString() << std::endl;
}

void            OperationHandler::infoone() {
	if (this->_list.size() < 1)
		throw(OperationException("Infoone on empty stack"));
	if (this->_err)
		return ;

	const std::string	arr[] = {"int8", "int16", "int32", "float", "double"};

	std::cout << this->_list.back()->toString() << " and has type " << arr[this->_list.back()->getType()] << std::endl;
}

void            OperationHandler::info() {
	if (this->_list.size() < 1)
		throw(OperationException("Info on empty stack"));
	if (this->_err)
		return ;

	const std::string	arr[] = {"int8", "int16", "int32", "float", "double"};

	for (auto i = this->_list.rbegin(); i != this->_list.rend(); ++i)
		std::cout << (*i)->toString() << " and has type " << arr[(*i)->getType()] << std::endl;
}

void            OperationHandler::uptype() {
	if (this->_list.size() < 1)
		throw(OperationException("Uptype on empty stack"));

	IOperand const	*ptr = this->_list.back();

	if (ptr->getType() == DOUBLE)
		throw(OperationException("Type DOUBLE cannot become bigger using uptype"));
	if (this->_err)
		return ;

	eOperandType	type = ptr->getType();
	if (type == INT8)
		type = INT16;
	else if (type == INT16)
		type = INT32;
	else if (type == INT32)
		type = FLOAT;
	else if (type == FLOAT)
		type = DOUBLE;

	IOperand const	*newptr = createOperand(type, ptr->toString());

	delete ptr;
	this->_list.pop_back();
	this->_list.push_back(newptr);
}
