#include "Int32.hpp"
#include "AbstractVM.hpp"
#include <exception>
#include <limits>
#include "OperationException.hpp"
#include "OperationTemplates.hpp"

Int32::Int32(std::string const &num) : _type(INT32) {
	long	integer = std::strtol(num.c_str(), NULL, 10);

	if (errno == ERANGE)
	{
		if (integer == LONG_MAX)
			throw (OperationException::Overflow());
		else if (integer == LONG_MIN)
			throw (OperationException::Underflow());
	}
	if (integer > std::numeric_limits<int>::max())
		throw (OperationException::Overflow());
	else if (integer < std::numeric_limits<int>::min())
		throw (OperationException::Underflow());
	this->_number = std::to_string(integer);
}

Int32::Int32(Int32 const &obj) : _type(INT32), _number(obj.toString()) {}

Int32::~Int32() {}

int					Int32::getPrecision() const {
	return (this->_type);
}

eOperandType		Int32::getType() const {
	return (this->_type);
}

IOperand const		*Int32::operator+(IOperand const &obj) const {
	int		p1 = std::stoi(this->toString());
	int		p2 = std::stoi(obj.toString());
	int		sum;

	sum = add(p1, p2);
	return (createOperand(INT32, std::to_string(sum)));
}

IOperand const		*Int32::operator-(IOperand const &obj) const {
	int		p1 = std::stoi(this->toString());
	int		p2 = std::stoi(obj.toString());
	int		sum;

	sum = sub(p1, p2);
	return (createOperand(INT32, std::to_string(sum)));
}

IOperand const		*Int32::operator*(IOperand const &obj) const {
	int		p1 = std::stoi(this->toString());
	int		p2 = std::stoi(obj.toString());
	int		sum;

	sum = mul(p1, p2);
	return (createOperand(INT32, std::to_string(sum)));
}

IOperand const		*Int32::operator/(IOperand const &obj) const {
	int		p1 = std::stoi(this->toString());
	int		p2 = std::stoi(obj.toString());
	int		sum;

	sum = divka(p1, p2);
	return (createOperand(INT32, std::to_string(sum)));
}

IOperand const		*Int32::operator%(IOperand const &obj) const {
	int		p1 = std::stoi(this->toString());
	int		p2 = std::stoi(obj.toString());
	int		sum;

	sum = mod(p1, p2);
	return (createOperand(INT32, std::to_string(sum)));
}

std::string const	&Int32::toString(void) const {
	return (this->_number);
}
