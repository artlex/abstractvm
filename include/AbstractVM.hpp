/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AbstractVM.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/12 17:04:56 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/14 13:13:50 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ABSTRACTVM_HPP
# define ABSTRACTVM_HPP

# include <iostream>
# include <cerrno>

class IOperand;

enum eOperandType
{
	INT8,
	INT16,
	INT32,
	FLOAT,
	DOUBLE
};

const IOperand	*createOperand(eOperandType type, std::string const &value);

#endif
