#ifndef OPERATIONEXCEPTION_HPP
# define OPERATIONEXCEPTION_HPP
       
# include <exception>
# include <iostream>

class OperationException : public std::exception
{
public:
	OperationException(std::string const &);
	OperationException(OperationException const &);
	~OperationException() throw();

	OperationException	&operator=(OperationException const &);

	const char			*what() const throw();

	class DivisionByZero;

	class Overflow;

	class Underflow;

	class WrongNumber;

	class EmptyStack;

	class AssertError;

	class PrintError;

private:
	OperationException();

	const std::string		_str;
};

class OperationException::DivisionByZero : public std::exception {
public:
	DivisionByZero();
	DivisionByZero(DivisionByZero const &);
	~DivisionByZero() throw();

	DivisionByZero	&operator=(DivisionByZero const &);

	const char		*what() const throw() ;

private:

	const std::string		_str;
};

class OperationException::Overflow : public std::exception {
public:
	Overflow();
	Overflow(Overflow const &);
	~Overflow() throw();

	Overflow	&operator=(Overflow const &);

	const char	*what() const throw() ;

private:
	const std::string		_str;
};

class OperationException::Underflow : public std::exception {
public:
	Underflow();
	Underflow(Underflow const &);
	~Underflow() throw();

	Underflow	&operator=(Underflow const &);

	const char	*what() const throw() ;

private:
	const std::string		_str;
};

class OperationException::WrongNumber : public std::exception {
public:
	WrongNumber();
	WrongNumber(WrongNumber const &);
	~WrongNumber() throw();

	WrongNumber	&operator=(WrongNumber const &);

	const char	*what() const throw() ;

private:
	const std::string		_str;
};

class OperationException::EmptyStack : public std::exception {
public:
	EmptyStack();
	EmptyStack(EmptyStack const &);
	~EmptyStack() throw();

	EmptyStack	&operator=(EmptyStack const &);

	const char	*what() const throw() ;

private:
	const std::string		_str;
};

class OperationException::AssertError : public std::exception {
public:
	AssertError();
	AssertError(AssertError const &);
	~AssertError() throw();

	AssertError	&operator=(AssertError const &);

	const char	*what() const throw() ;

private:
	const std::string		_str;
};

class OperationException::PrintError : public std::exception {
public:
	PrintError();
	PrintError(PrintError const &);
	~PrintError() throw();

	PrintError	&operator=(PrintError const &);

	const char	*what() const throw() ;

private:
	const std::string		_str;
};

#endif
