#ifndef FLOAT_HPP
# define FLOAT_HPP

# include "IOperand.hpp"

class Float : public IOperand
{
public:
	Float(std::string const &num);

	Float(Float const &obj);

	virtual ~Float();

	virtual int					getPrecision(void) const; // Precision of the type of the instance
	virtual eOperandType		getType(void) const; // Type of the instance
	virtual IOperand const		*operator+(IOperand const & rhs) const; // Sum
	virtual IOperand const		*operator-(IOperand const & rhs) const; // Difference
	virtual IOperand const		*operator*(IOperand const & rhs) const; // Product
	virtual IOperand const		*operator/(IOperand const & rhs) const; // Quotient
	virtual IOperand const		*operator%(IOperand const & rhs) const; // Modulo
	virtual std::string const	&toString(void) const; // String representation of the instance

private:
	Float();
	Float						&operator=(Float const &obj);

	const eOperandType	_type;
	std::string			_number;
};

#endif
