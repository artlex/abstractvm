#ifndef DOUBLE_HPP
# define DOUBLE_HPP

# include "IOperand.hpp"

class Double : public IOperand
{
public:
	Double(std::string const &num);

	Double(Double const &obj);

	virtual ~Double();

	virtual int					getPrecision(void) const; // Precision of the type of the instance
	virtual eOperandType		getType(void) const; // Type of the instance
	virtual IOperand const		*operator+(IOperand const & rhs) const; // Sum
	virtual IOperand const		*operator-(IOperand const & rhs) const; // Difference
	virtual IOperand const		*operator*(IOperand const & rhs) const; // Product
	virtual IOperand const		*operator/(IOperand const & rhs) const; // Quotient
	virtual IOperand const		*operator%(IOperand const & rhs) const; // Modulo
	virtual std::string const	&toString(void) const; // String representation of the instance

private:
	Double();
	Double						&operator=(Double const &obj);

	const eOperandType	_type;
	std::string			_number;
};

#endif
