#ifndef INPUTREAD_HPP
# define INPUTREAD_HPP

#include <string>
#include <list>

class InputRead
{
public:
	struct s_line;

	InputRead();
	~InputRead();
	
	bool				getInput(const char *str);
	bool				getInput();

	bool	            getLine(InputRead::s_line &str);

private:

	class   Lexer;

	InputRead(const InputRead &);
	InputRead	operator=(const InputRead &);

	std::string		reconstructString(std::string const &str) const;

	std::list<s_line>	                        _list;
	typename std::list<s_line>::const_iterator  _iter;
};

struct  InputRead::s_line {
	std::string		str;
	std::size_t		line;
	bool			empty;
};

class   InputRead::Lexer {
public:

	~Lexer();

	static bool     hasMistakes();
	static bool     validate(std::string const &str, std::size_t len);
	static bool		deepCheck(std::string const &str, std::size_t line);

private:

	Lexer();
	Lexer(Lexer const &);
	Lexer   &operator=(Lexer const &);

	static bool     _hasMistakes;
	static bool		_exit;

};

typedef InputRead::s_line   Line;

#endif
