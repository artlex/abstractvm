/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OperationTemplates.hpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/09 16:05:57 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/12 16:37:13 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERATIONTEMPLATES_HPP
# define OPERATIONTEMPLATES_HPP

#include <fenv.h>
#include <limits>

template <class T>
T			add(T p1, T p2) {
	T		sum;

	if (p2 > 0 && p1 > std::numeric_limits<T>::max() - p2)
		throw (OperationException::Overflow());
	else if (p2 < 0 && p1 < std::numeric_limits<T>::min() - p2)
		throw (OperationException::Underflow());
	sum = p1 + p2;
	return (sum);
}

template <class T>
T			sub(T p1, T p2) {
	T		sum;

	if (p2 < 0 && p1 > std::numeric_limits<T>::max() + p2)
		throw (OperationException::Overflow());
	else if (p2 > 0 && p1 < std::numeric_limits<T>::min() + p2)
		throw (OperationException::Underflow());
	sum = p1 - p2;
	return (sum);
}

template <class T>
T			mul(T p1, T p2) {
	T		sum;

	if ((p1 == -1 && p2 == std::numeric_limits<T>::min()) ||
			(p2 == -1 && p1 == std::numeric_limits<T>::min()))
		throw (OperationException::Overflow());
	else if (p2 == -1 || p2 == 0)
		sum = 0;
	else if (p1 > std::numeric_limits<T>::max() / p2)
		throw (OperationException::Overflow());
	else if (p1 < std::numeric_limits<T>::min() / p2)
		throw (OperationException::Underflow());
	sum = p1 * p2;
	return (sum);
}

template <class T>
T			divka(T p1, T p2) {
	T		sum;

	if (p2 == 0)
		throw (OperationException::DivisionByZero());
	if (p2 == -1 && p2 == std::numeric_limits<T>::min())
		throw (OperationException::Overflow());
	sum = p1 / p2;
	return (sum);
}

template <class T>
T			mod(T p1, T p2) {
	T		sum;

	if (p2 == 0)
		throw (OperationException::DivisionByZero());
	if (p2 == -1 && p2 == std::numeric_limits<T>::min())
		throw (OperationException::Overflow());
	sum = p1 % p2;
	return (sum);
}

/*
** Float specialization
*/

inline float		add(float p1, float p2) {
	float		sum;

	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 + p2;
    if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

inline float		sub(float p1, float p2) {
	float		sum;

	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 - p2;
    if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

inline float		mul(float p1, float p2) {
	float		sum;

	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 * p2;
    if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

inline float		divka(float p1, float p2) {
	float		sum;

	if (p2 == 0)
		throw(OperationException::DivisionByZero());
	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 / p2;
	if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

/*
** Double specialization 
*/

inline double		add(double p1, double p2) {
	double		sum;

	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 + p2;
    if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

inline double		sub(double p1, double p2) {
	double		sum;

	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 - p2;
    if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

inline double		mul(double p1, double p2) {
	double		sum;

	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 * p2;
    if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

inline double		divka(double p1, double p2) {
	double		sum;

	if (p2 == 0)
		throw(OperationException::DivisionByZero());
	feclearexcept(FE_ALL_EXCEPT);
	sum = p1 / p2;
	if (fetestexcept(FE_OVERFLOW))
		throw(OperationException::Overflow());
    else if (fetestexcept(FE_UNDERFLOW))
		throw(OperationException::Underflow());
	return (sum);
}

#endif
