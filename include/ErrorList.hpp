#ifndef ERRORLIST_HPP
# define ERRORLIST_HPP

#include <list>
#include <string>
#include <sstream>
#include <regex>
#include <iostream>

class ErrorList
{
public:
	static ErrorList	*getErrorList();
	~ErrorList();

	void				push(std::string const &str);
	void                printErrors() const;

private:
	ErrorList();
	ErrorList(const ErrorList &);
	ErrorList	&operator=(const ErrorList &);

	std::list<std::string>	_list;
	static ErrorList		*_ptr;
};

inline void					createBoldText(std::string &str)
{
	const int			elems = 16;
	const std::string	arr[] = {"pop", "dump", "add", "sub", "mul", "div",
								"mod", "print", "exit", "push", "assert", "clear",
								"dumpone", "infoone", "info", "uptype"};
	std::size_t			pos(0);
	int					i(0);

	if (!regex_match(str, std::regex(".*(pop|dump|add|sub|mul|div|mod|print|exit|push|assert|clear|dumpone|infoone|info|uptype).*")))
		return ;
	for (i = 0; i != elems; ++i)
		if ((pos = str.find(arr[i], 0)) != std::string::npos)
			break ;
	str.erase(pos, arr[i].size());
	str.insert(pos, "\x1b[1m" + arr[i] + "\x1b[0m");
}

inline void                 addError(std::string const &str, std::size_t line, std::string const &text) {
	ErrorList               *list = ErrorList::getErrorList();
	std::stringstream       stream;
	std::string				new_text(text);

	createBoldText(new_text);
	stream << str << " on line " << line << ": " << std::endl << "\t" << new_text << std::endl;
	list->push(stream.str());
}

inline void                 ExitError() {
	ErrorList               *list = ErrorList::getErrorList();
	std::stringstream       stream;

	stream << "Exit command not found" << std::endl;
	list->push(stream.str());
}

inline void                 printErrors() {
	ErrorList               *list = ErrorList::getErrorList();

	list->printErrors();
}

#endif
