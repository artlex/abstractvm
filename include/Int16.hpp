#ifndef INT16_HPP
# define INT16_HPP

# include "IOperand.hpp"

class Int16 : public IOperand
{
public:
	Int16(std::string const &num);

	Int16(Int16 const &obj);

	virtual ~Int16();

	virtual int					getPrecision(void) const; // Precision of the type of the instance
	virtual eOperandType		getType(void) const; // Type of the instance
	virtual IOperand const		*operator+(IOperand const & rhs) const; // Sum
	virtual IOperand const		*operator-(IOperand const & rhs) const; // Difference
	virtual IOperand const		*operator*(IOperand const & rhs) const; // Product
	virtual IOperand const		*operator/(IOperand const & rhs) const; // Quotient
	virtual IOperand const		*operator%(IOperand const & rhs) const; // Modulo
	virtual std::string const	&toString(void) const; // String representation of the instance

private:
	Int16();
	Int16						&operator=(Int16 const &obj);

	const eOperandType	_type;
	std::string			_number;
};

#endif
