#ifndef INT32_HPP
# define INT32_HPP

# include "IOperand.hpp"

class Int32 : public IOperand
{
public:
	Int32(std::string const &num);

	Int32(Int32 const &obj);

	virtual ~Int32();

	virtual int					getPrecision(void) const; // Precision of the type of the instance
	virtual eOperandType		getType(void) const; // Type of the instance
	virtual IOperand const		*operator+(IOperand const & rhs) const; // Sum
	virtual IOperand const		*operator-(IOperand const & rhs) const; // Difference
	virtual IOperand const		*operator*(IOperand const & rhs) const; // Product
	virtual IOperand const		*operator/(IOperand const & rhs) const; // Quotient
	virtual IOperand const		*operator%(IOperand const & rhs) const; // Modulo
	virtual std::string const	&toString(void) const; // String representation of the instance

private:
	Int32();
	Int32						&operator=(Int32 const &obj);

	const eOperandType	_type;
	std::string			_number;
};

#endif
