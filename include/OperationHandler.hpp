/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OperationHandler.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 16:49:57 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/12 18:44:26 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ABSTRACTVM_OPERATIONHANDLER_HPP
# define ABSTRACTVM_OPERATIONHANDLER_HPP

# include <iostream>
# include <list>
# include "InputRead.hpp"

class IOperand;

class OperationHandler {

public:
	OperationHandler();
	~OperationHandler();

	bool        wasExit();
	bool		wasErr();

	void        push(std::string const &str);
	void        assert(std::string const &str);
	void        pop();
	void        dump();
	void        add();
	void        sub();
	void        mul();
	void        div();
	void        mod();
	void        print();
	void        exit();

	void		clear();
	void		dumpone();
	void		infoone();
	void		info();
	void		uptype();

	void        factory(Line &line);

private:

	typedef std::list<const IOperand *>	t_lst;

	OperationHandler(const OperationHandler &);
	OperationHandler &operator=(const OperationHandler &);

	void        	_getNumbers(IOperand const *&p1, IOperand const *&p2) const;
	IOperand const	*_changePrecision(IOperand const *p1, IOperand const *p2) const;
	void			_updateStack(IOperand const *&num);

	t_lst   						  _list;
	bool                            _exit;
	bool							_err;
};

#endif
