#ifndef INT8_HPP
# define INT8_HPP

# include "IOperand.hpp"

class Int8 : public IOperand
{
public:
	Int8(std::string const &num);
	Int8(Int8 const &obj);

	virtual ~Int8();

	virtual int					getPrecision(void) const; // Precision of the type of the instance
	virtual eOperandType		getType(void) const; // Type of the instance
	virtual IOperand const		*operator+(IOperand const & rhs) const; // Sum
	virtual IOperand const		*operator-(IOperand const & rhs) const; // Difference
	virtual IOperand const		*operator*(IOperand const & rhs) const; // Product
	virtual IOperand const		*operator/(IOperand const & rhs) const; // Quotient
	virtual IOperand const		*operator%(IOperand const & rhs) const; // Modulo
	virtual std::string const	&toString(void) const; // String representation of the instance

private:
	Int8();
	Int8						&operator=(Int8 const &obj);

	const eOperandType	_type;
	std::string			_number;
};

#endif
