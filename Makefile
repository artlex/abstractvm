.PHONY: all clean fclean re

CC=g++
CFLAGS=-Wall -Wextra -Werror -Iinclude -std=c++11
NAME=avm
OBJDIR=obj
SRCDIR=src

SRC=Double.cpp Factory.cpp Int16.cpp Int8.cpp main.cpp ErrorList.cpp Float.cpp InputRead.cpp Int32.cpp OperationException.cpp OperationHandler.cpp

OBJ=$(SRC:.cpp=.o)

all: $(OBJDIR) $(NAME)

$(OBJDIR):
	@echo "Compiling AbstactVM"
	@mkdir -p $@

$(NAME): $(addprefix $(OBJDIR)/, $(OBJ))
	@$(CC) $(CFLAGS) -o $@ $^
	@echo "\nAbstractVM compiled"

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.cpp)
	@printf "."
	@$(CC) $(CFLAGS) -c -o $@ $<

debug:
	$(CC) $(CFLAGS) $(addprefix $(SRCDIR)/, $(SRC)) -o $(NAME) -g

clean:
	@rm -rf $(OBJDIR)
	@echo "cleaned"

fclean:
	@rm -rf $(OBJDIR)
	@rm -f $(NAME)
	@echo "fcleaned"

re: fclean all
